#include <iostream>
#include <string>
#include <memory>

using namespace std;



class binary_tree{

//The node of a tree
struct node
{
	//Data stored in this node of the tree
	int data;
	//The left branch of the tree
	node *left;
	//The right branch of the tree
	node *right;
};

node *root = NULL;

public:
	
	void insert(int value);
	void insert(node **tree, int value);
	void delete_tree(node *tree);
	void delete_tree();
	bool exists(node *tree, int value);
	bool exists(int value);
	void remove(node *&tree, int value);
	void remove(int value);
	binary_tree();
	binary_tree(int value);
	//binary_tree(const vector<int> &values);
	binary_tree(const binary_tree &rhs);
	node operator=(const node &rhs);
	void inorder();
	void preorder();
	void postorder();

private:
	void inorder(node *tree);
	void preorder(node *tree);
	void postorder(node *tree);

protected:
	node *tree;
};

//Creates an empty binary tree if no details are given

binary_tree::binary_tree()
{
	tree = nullptr;
}

//Creates a binary tree with an initial value

binary_tree::binary_tree(int value)
{
	tree = new node;
	(tree)->data = value;
}

//Inserts a value into the tree
void binary_tree::insert(node **tree, int value)
{
	//Check if nullptr. If so set new node
	if (*tree == nullptr)
	{
		//Create new node
		*tree = new node;
		//Set new value
		(*tree)->data = value;
		//Set branches to nullptr
		(*tree)->left = nullptr;
		(*tree)->right = nullptr;
	}
	else if (value < (*tree)->data)
	{
		insert(&(*tree)->left, value);
	}
	else if (value >(*tree)->data)
	{
		insert(&(*tree)->right, value);
	}
	else
	{
		return;
	}
}

void binary_tree::insert(int value)
{
	insert(&root, value);
}

//Deletes the tree - freeing memory
void binary_tree::delete_tree(node *tree)
{
	if (tree == nullptr)
	{
		return;
	}
	delete_tree((tree)->left);
	delete_tree((tree)->right);
}

void binary_tree::delete_tree()
{
	delete_tree(root);
}

//Prints the tree in order
void binary_tree::inorder(node *tree)
{
	if (tree != nullptr)
	{

		inorder((tree)->left);
		cout << (tree)->data << ", ";
		inorder((tree)->right);
	}
}

void binary_tree::inorder()
{
	inorder(root);
}

//Prints the tree from left to right

void binary_tree::preorder(node *tree)
{
	if (tree != nullptr)
	{
		cout << (tree)->data << ", ";
		preorder((tree)->left);
		preorder((tree)->right);
	}
}

void binary_tree::preorder()
{
	preorder(root);
}

//Prints the tree from right to left

void binary_tree::postorder(node *tree)
{
	if (tree != nullptr)
	{
		postorder((tree)->left);
		postorder((tree)->right);
		cout << (tree)->data << ", ";
	}
}

void binary_tree::postorder()
{
	postorder(root);
}

//Returns true if the value is in the tree

bool binary_tree::exists(node *tree, int value)
{
	if (tree != nullptr)
	{
		if ((tree)->data == value)
			return 1;
		else if (exists((tree)->left, value) == 1)
			return 1;
		else if (exists((tree)->right, value) == 1)
			return 1;
	}

	return 0;
}

bool binary_tree::exists(int value)
{
	return (exists(root, value));
}

//Attempts to find the value in the tree, and if it finds it, it removes it

void binary_tree::remove(node *&tree, int value)
{
	node *currentNode;
	node *parentNode;

	if (tree == nullptr)
	{
		cout << "Cannot find value." << endl;
	}
	else if ((tree)->data == value && ((((tree)->left == nullptr)) && ((tree)->right == nullptr)))
	{
		tree = nullptr;
	}
	else if ((tree)->data == value && ((((tree)->left != nullptr)) && ((tree)->right == nullptr)))
	{
		tree = (tree)->left;
	}
	else if ((tree)->data == value && ((((tree)->left == nullptr)) && ((tree)->right != nullptr)))
	{
		tree = (tree)->right;
	}
	else if ((tree)->data == value && ((((tree)->left != nullptr)) && ((tree)->right != nullptr)))
	{
		currentNode = (tree)->left;
		parentNode = NULL;

		while ((currentNode)->right != NULL)
		{
			parentNode = currentNode;
			currentNode = (currentNode)->right;
		}
		(tree)->data = (currentNode)->data;

		if (parentNode == NULL)
			(tree)->left = (currentNode)->left;
		else
			(parentNode)->right = (currentNode)->right;
		delete currentNode;
	}

	else if (value < (tree)->data)
	{
		remove((tree)->left, value);
	}
	else if (value > (tree)->data)
	{
		remove((tree)->right, value);
	}

}

void binary_tree::remove(int value)
{
	remove(root, value);
}