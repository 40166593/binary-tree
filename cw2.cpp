/*
Coded by Michael G (40166593)

*/

#include <iostream>
#include <string>
#include <memory>
#include "binTree.h"

using namespace std;

int main(int argc, char **argv)
{
	//Declare the tree
	binary_tree *tree;
	tree = new binary_tree();

	//Declare the variable to hold the root value;
	int root;

	//Declare the variable to hold which setting the menu is choosing;
	int setting = 0;

	cout << "Enter root number: ";
	cin >> root;

	//Inserts initial value to the tree
	tree->insert(root);

	while (setting != 5){

		cout << "Enter the setting you wish to run." << endl;
		cout << "1. Insert a value to the tree." << endl;
		cout << "2. Remove a value from the tree." << endl;
		cout << "3. Search the tree for a value." << endl;
		cout << "4. Print the tree." << endl;
		cout << "5. Exit." << endl;

		cin >> setting;

		switch (setting)
		{
		case 1:
			int num;
			cout << "Enter number: ";
			cin >> num;
			tree->insert(num);
			break;
		case 2:
			int valueToRemove;
			cout << "Enter a value you wish to remove: ";
			cin >> valueToRemove;
			tree->remove(valueToRemove);
			break;
		case 3:
			int valueToSearchFor;
			cout << "Enter a value you wish to search for: ";
			cin >> valueToSearchFor;

			if (tree->exists(valueToSearchFor))
				cout << "Tree contains " << valueToSearchFor << endl;
			else
				cout << "Tree does not contain " << valueToSearchFor << endl;
			break;
		case 4:
			int mode;
			cout << "Enter the mode you wish to use. 1: In Order. 2: Pre-Order. 3: Post-Order" << endl;
			cin >> mode;
			if (mode < 1 || mode > 4)
			{
				cout << "The mode you have entered is invalid." << endl;
				break;
			}
			if (mode == 1)
				tree->inorder();
			else if (mode == 2)
				tree->preorder();
			else if (mode == 3)
				tree->postorder();
			else
				cout << "Mode not recognised  ";

			cout << "\b\b " << endl;

			cout << endl;
			break;
		case 5:
			cout << "Exiting..." << endl;
			//Deletes the tree for memory management
			tree->delete_tree();
			return 0;
			break;
		defult:
			cout << "Entry not recognised." << endl;
		}
	}
}