Created by Michael Gauld 40166593

The toolchain used to build this application is the Microsoft Compiler (CL).

Make file:
- makeall: compiles the cpp into an exe.
- clean: removes all .exe, .asm and .obj for the directory.
- run: runs the .exe file.

Interface:
Use the number keys to navigate the menus using the command prompts input.